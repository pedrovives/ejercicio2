<?php
require_once 'SelectorIndividual.php';

class SIRadioOpcion extends SelectorIndividual
{

    public function  generaSelector(): string
    {
        $i = 0;

        $selector="<label>$this->titulo</label>";

        foreach ($this->elementos as $clave => $valor)
        {
            if ($this->seleccionado === $i)
                $seleccionado = 'checked';
            else
                $seleccionado = '';

            $selector .= sprintf(
                "<label><input type='radio' name='%s' value='%s' %s>%s</label>",
                $this-> nombre,
                $clave,
                $seleccionado,
                $valor
            );

            $i++;
        }
        return $selector;
    }
}